#include <alloca.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "crappy_sort.h"
#include "sort_bench.h"

#define ARRAY_LEN_ 1000000 // (1024 * 1022)  // (20 * 5)

/*
#include <stdlib.h>

int rand(void);
void srand(unsigned int seed);
*/

int main(/* int argc, char const* argv[] */)
{
    /*  uint64_t* tab = alloca(sizeof(uint64_t) * ARRAY_LEN_);

    crappysort_FillRandom(tab, ARRAY_LEN_, 100, 0);

    {
           for (size_t i = 0; i < ARRAY_LEN_; ++i)
        {
            if (i % 20 == 0)
            {
                puts("");
            }
            printf("%ld\t", tab[i]);
        }

        puts("");

        printf("-> Is sorted?: %s\n",
               crappysort_IsSorted(tab,
                                   ARRAY_LEN_,
                                   crappysort_CompareUINT64EvenFirst)
                   ? "True"
                   : "False");
    }

    {
        crappysort_IntroSort(tab,
                             ARRAY_LEN_,
                             crappysort_CompareUINT64EvenFirst);


        for (size_t i = 0; i < ARRAY_LEN_; ++i)
        {
            if (i % 20 == 0)
            {
                puts("");
            }
            printf("%ld\t", tab[i]);
        }

        puts("");


        printf("-> Is sorted?: %s\n",
               crappysort_IsSorted(tab,
                                   ARRAY_LEN_,
                                   crappysort_CompareUINT64EvenFirst)
                   ? "True"
                   : "False");
    } */

    sort_bench_Ctx_t ctx;

    if(sort_bench_InitCtx(&ctx, crappysort_QuickSort, crappysort_CompareUINT64, ARRAY_LEN_) < 0)
    {
        return -1;
    }

    puts("Benchmark on crappysort_CompareUINT64");

    {
        puts("\nBenchmark for crappysort_QuickSort:");
        sort_bench_Run(&ctx);
    }
    {
        sort_bench_ChangeSort(&ctx, crappysort_IntroSort);
        puts("\nBenchmark for crappysort_IntroSort:");
        sort_bench_Run(&ctx);
    }
    {
        sort_bench_ChangeSort(&ctx, crappysort_BubbleSort);
        puts("\nBenchmark for crappysort_BubbleSort:");
        sort_bench_Run(&ctx);
    }
    {
        sort_bench_ChangeSort(&ctx, crappysort_InsertionSort);
        puts("\nBenchmark for crappysort_InsertionSort:");
        sort_bench_Run(&ctx);
    }

    sort_bench_ResetCtx(&ctx);

    return 0;
}
