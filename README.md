# Crappysort
A crappy sort library for testing sort algorithms.

## To compile you might need:
cmake

gcc > 4 or clang

Its currently supported only on linux.

## Compile:

$ mkdir build && cd build

$ cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..

$ cmake --build . -- -j 16 # or $ make -j 16

## Generate the doc

$ mkdir docs

$ doxygen doxyMain.doxy

The generated doc start at "docs/html/index.html"

## Usage:

./app_tp3 <path_to_file_to_check>
