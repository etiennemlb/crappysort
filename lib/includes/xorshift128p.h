/**
 * @file xorshift128p.h
 * @author Wikipediq
 * @brief Crappy xorshift128+ implementation.
 * @version 0.1
 * @date 2020-02-15
 *
 */

#ifndef XORSHIFT128P_H
#define XORSHIFT128P_H

#include <stdint.h>

typedef struct
{
    uint64_t a;
    uint64_t b;
} xorshift128p_State_t;

/**
 * @brief Initialize a xorshift128+ state.
 *
 * @param state valid state ptr
 * @return int -1 if error, else 0. errno set appropriatly.
 */
int xorshift128p_InitState(xorshift128p_State_t* state);

/**
 * @brief Bad implementation of xorshift128+.
 *
 * @param state non zero state.
 * @return uint64_t pr generated number.
 */
uint64_t xorshift128p_NextLong(xorshift128p_State_t* state);

#endif