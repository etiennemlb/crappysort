/**
 * @file crappy_sort.h
 * @author E. Malaboeuf
 * @brief
 * @version 0.1
 * @date 2020-02-15
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef CRAPPY_SORT_H
#define CRAPPY_SORT_H

#include <stdint.h>
#include <stdlib.h>

/**
 * @brief
 *
 */
typedef int (*crappysort_Comparator_f)(uint64_t, uint64_t);

typedef void (*crappysort_Sort_f)(uint64_t*, size_t, crappysort_Comparator_f);

/**
 * @brief check if an array is sorted
 *
 * @param data a valid data array ptr
 * @param len the len of the array
 * @param comparator a comparator
 * @return int 0 if it is not sorted, else 1 if it is
 */
int crappysort_IsSorted(uint64_t* data, size_t len, crappysort_Comparator_f comparator);

/**
 * @brief OSEF
 *
 * @param data valid data array
 * @param len len of data
 * @param max max number generated
 * @param min min number generated
 */
void crappysort_FillRandom(uint64_t* data, size_t len, size_t max, size_t min);

/**
 * @brief Compare two values.
 *
 * @param a
 * @param b
 * @return int less than 0 if a is less than b. more than 0 if a is greate than b.
 */
int crappysort_CompareUINT64(uint64_t a, uint64_t b);

/**
 * @brief
 *
 * @param b
 * @param a
 * @return int
 */
int crappysort_CompareUINT64Reverse(uint64_t b, uint64_t a);

/**
 * @brief
 *
 * @param a
 * @param b
 * @return int
 */
int crappysort_CompareUINT64EvenFirst(uint64_t a, uint64_t b);

/**
 * @brief
 *
 * @param a
 * @param b
 * @return int
 */
int crappysort_CompareUINT64EvenLast(uint64_t a, uint64_t b);

/**
 * @brief
 *
 * @param data
 * @param len
 * @param comparator
 * @return int64_t
 */
int64_t crappysort_Partition(uint64_t* data, size_t low, size_t high, crappysort_Comparator_f comparator);

/**
 * @brief
 * Slow as fuck.
 * Better https://code.woboq.org/userspace/glibc/stdlib/qsort.c.html#3size
 * @param data
 * @param len
 * @param comparator
 */
void crappysort_QuickSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator);

/**
 * @brief
 *
 * @param data
 * @param len
 * @param comparator
 */
void crappysort_BubbleSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator);

/**
 * @brief quicksort for large arrays and insertion sort for smaller ones.
 * Slow as fuck.
 * Better : https://code.woboq.org/userspace/glibc/stdlib/qsort.c.html#3size
 * @param data
 * @param len
 * @param comparator
 */
void crappysort_IntroSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator);

/**
 * @brief
 *
 * @param data
 * @param len
 * @param comparator
 */
void crappysort_InsertionSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator);

#endif
