/**
 * @file sort_bench.h
 * @author E. Malaboeuf
 * @brief
 * @version 0.1
 * @date 2020-02-15
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef CRAPPY_SORT_BENCHMARK_H
#define CRAPPY_SORT_BENCHMARK_H

#include <stdint.h>
#include <sys/types.h>

#include "crappy_sort.h"

typedef struct
{
    uint64_t* data_;
    size_t    len_;

    crappysort_Sort_f       sort_;
    crappysort_Comparator_f comparator_;

} sort_bench_Ctx_t;

/**
 * @brief
 *
 * @param ctx
 * @param sort
 * @param comparator
 * @param len
 * @return int
 */
int sort_bench_InitCtx(sort_bench_Ctx_t* ctx, crappysort_Sort_f sort, crappysort_Comparator_f comparator, size_t len);

/**
 * @brief
 *
 * @param ctx
 * @param comparator
 * @return int
 */
int sort_bench_ChangeSort(sort_bench_Ctx_t* ctx, crappysort_Sort_f sort);

/**
 * @brief
 *
 * @param ctx
 */
void sort_bench_Run(sort_bench_Ctx_t* ctx);

/**
 * @brief
 *
 * @param ctx
 */
void sort_bench_ResetCtx(sort_bench_Ctx_t* ctx);

#endif
