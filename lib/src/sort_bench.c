#define _POSIX_C_SOURCE 201100L

#include "sort_bench.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#define SORT_BENCHMARK_DEFAULT_CLOCK CLOCK_MONOTONIC

int sort_bench_InitCtx(sort_bench_Ctx_t* ctx, crappysort_Sort_f sort, crappysort_Comparator_f comparator, size_t len)
{
    if(ctx == NULL || sort == NULL || comparator == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    ctx->len_        = len;
    ctx->sort_       = sort;
    ctx->comparator_ = comparator;

    if((ctx->data_ = malloc(sizeof(uint64_t) * ctx->len_)) == NULL)
    {
        return -1;
    }

    crappysort_FillRandom(ctx->data_, ctx->len_, 0, 1 << 20);

    return 0;
}

size_t sort_bench_GetCurrentTimeNs()
{
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    return now.tv_sec * 1000000000 + now.tv_nsec;
}

int sort_bench_ChangeSort(sort_bench_Ctx_t* ctx, crappysort_Sort_f sort)
{
    if(ctx == NULL || sort == NULL)
    {
        return -1;
    }

    ctx->sort_ = sort;

    return 0;
}

/*
int crappysort_CompareUINT64_STRQsort(const void* a, const void* b)
{
    return ((uint64_t)b) - ((uint64_t)a);
}
*/

void sort_bench_Run(sort_bench_Ctx_t* ctx)
{
    if(ctx == NULL)
    {
        return;
    }

    uint64_t* data_tmp_;

    if((data_tmp_ = malloc(sizeof(uint64_t) * ctx->len_)) == NULL)
    {
        free(ctx->data_);
        return;
    }

    size_t start;
    size_t end;

    size_t time_sum    = 0;
    size_t loop_count  = 0;
    size_t old_average = 0;

    ssize_t time_sum_delta            = 0;
    ssize_t average_delta_average     = ~(-1);
    ssize_t old_average_delta_average = 0;

    do
    {
        memcpy(data_tmp_, ctx->data_, sizeof(uint64_t) * ctx->len_);

        start = sort_bench_GetCurrentTimeNs();
        {
            ctx->sort_(data_tmp_, ctx->len_, ctx->comparator_);

            /* qsort(data_tmp_, ctx->len_, sizeof(*data_tmp_), crappysort_CompareUINT64_STRQsort); */
        }
        end = sort_bench_GetCurrentTimeNs();

        time_sum += end - start;
        ++loop_count;

        size_t new_average = time_sum / loop_count;

        size_t new_average_delta = new_average - old_average;
        old_average              = new_average;

        time_sum_delta += new_average_delta;

        old_average_delta_average = average_delta_average;
        average_delta_average     = time_sum_delta / (loop_count * loop_count);

        /* printf("%ld\n", new_average); */
    } while(abs(old_average_delta_average - average_delta_average) > (average_delta_average >> 6)); // 1 %

    printf("Average time: %ld µs\n", old_average / 1000);

    /*
    for (size_t i = 0; i < ctx->len_; ++i)
    {
        if (i % 20 == 0)
        {
            puts("");
        }
        printf("%ld\t", data_tmp_[i]);
    }

    puts("");
    */

    free(data_tmp_);
}

void sort_bench_ResetCtx(sort_bench_Ctx_t* ctx)
{
    if(ctx == NULL)
    {
        return;
    }

    free(ctx->data_);
}
