#include "xorshift128p.h"

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

int xorshift128p_InitState(xorshift128p_State_t* state)
{
    if(state == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    state->a = 0xDEADC0DEDEADC0DE;
    state->b = 0xB16B00B5BABE0000;

    return 0;
}

/* The state must be seeded so that it is not all zero */
uint64_t xorshift128p_NextLong(xorshift128p_State_t* state)
{
    uint64_t       t = state->a;
    uint64_t const s = state->b;
    state->a         = s;
    t ^= t << 23;       // a
    t ^= t >> 17;       // b
    t ^= s ^ (s >> 26); // c
    state->b = t;
    return t + s;
}
