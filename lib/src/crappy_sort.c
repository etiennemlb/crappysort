#include "crappy_sort.h"

#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h> /* qsort */
#include <sys/types.h>

#include "xorshift128p.h"

int crappysort_IsSorted(uint64_t* data, size_t len, crappysort_Comparator_f comparator)
{
    if(data == NULL)
    {
        errno = EFAULT;
        return -1;
    }

    for(size_t i = 0; i < (len - 1); ++i)
    {
        if(comparator(data[i], data[i + 1]) > 0)
        {
            /* printf("data[i] = %ld, data[i + 1] = %ld\n", data[i], data[i + 1]); */
            return 0;
        }
    }

    return 1;
}

void crappysort_FillRandom(uint64_t* data, size_t len, size_t min, size_t max)
{
    static xorshift128p_State_t state;
    static char                 initialized = 0;

    if(data == NULL || max < min)
    {
        return;
    }

    if(initialized == 0)
    {
        xorshift128p_InitState(&state);
        initialized = 1;
    }

    for(size_t i = 0; i < len; ++i)
    {
        data[i] = (xorshift128p_NextLong(&state) % max) + min;
    }
}

int crappysort_CompareUINT64(uint64_t a, uint64_t b)
{
    return a - b;
}

int crappysort_CompareUINT64Reverse(uint64_t b, uint64_t a)
{
    return a - b;
}

int crappysort_CompareUINT64EvenFirst(uint64_t a, uint64_t b)
{
    int is_a_even = (a & 1) == 0;
    int is_b_even = (b & 1) == 0;

    return is_a_even - is_b_even;
}

int crappysort_CompareUINT64EvenLast(uint64_t a, uint64_t b)
{
    int is_a_even = (a & 1) == 0;
    int is_b_even = (b & 1) == 0;

    return is_b_even - is_a_even;
}

int64_t crappysort_Partition(uint64_t* data, size_t low, size_t high, crappysort_Comparator_f comparator)
{
    /*
    algorithm partition(A, lo, hi) is
        pivot := A[⌊(hi + lo) / 2⌋]
        i := lo - 1
        j := hi + 1
        loop forever
            do
                i := i + 1
            while A[i] < pivot
            do
                j := j - 1
            while A[j] > pivot
            if i ≥ j then
                return j
            swap A[i] with A[j]
    */

    uint64_t pivot = data[(high + low) / 2];

    size_t j = high + 1;
    size_t i = low - 1;

    for(;;)
    {
        do
        {
            ++i;
        } while(comparator(pivot, data[i]) > 0); /* data[i] < pivot */
        do
        {
            --j;
        } while(comparator(data[j], pivot) > 0); /* data[j] > pivot */

        if(i >= j)
        {
            return j;
        }

        /* swap */
        uint64_t tmp = data[i];
        data[i]      = data[j];
        data[j]      = tmp;
    }

    /* unreachable */
}

/* Quick sort */

void crappysort_QuickSortRec__(uint64_t* data, size_t low, size_t high, crappysort_Comparator_f comparator)
{
    /*
    if lo < hi then
        p : = partition(A, lo, hi)
            quicksort(A, lo, p)
            quicksort(A, p + 1, hi)
    */
    size_t pivot;
    if(low < high)
    {
        pivot = crappysort_Partition(data, low, high, comparator);

        crappysort_QuickSortRec__(data, low, pivot, comparator);
        crappysort_QuickSortRec__(data, pivot + 1, high, comparator);
    }
}

void crappysort_QuickSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator)
{
    if(data == NULL || comparator == NULL)
    {
        return;
    }

    crappysort_QuickSortRec__(data, 0, len - 1, comparator);
}
/* End quick sort */

/* Bubble sort */

void crappysort_BubbleSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator)
{
    if(data == NULL || comparator == NULL)
    {
        return;
    }

    /*
    procedure bubbleSort(A : list of sortable items)
        n := length(A)
        repeat
            newn := 0
            for i := 1 to n - 1 inclusive do
                if A[i - 1] > A[i] then
                    swap(A[i - 1], A[i])
                    newn := i
                end if
            end for
            n := newn
        until n ≤ 1
    end procedure
    */

    size_t high = len;
    do
    {
        size_t new_high = 0;

        for(size_t i = 1; i < high; ++i)
        {
            if(comparator(data[i - 1], data[i]) > 0)
            {
                uint64_t tmp = data[i - 1];

                data[i - 1] = data[i];
                data[i]     = tmp;

                new_high = i;
            }
        }

        high = new_high;

    } while(high > 1);
}

/* End bubble sort */

/* Intro sort */

void crappysort_IntroSortRec__(uint64_t*               data,
                               size_t                  low,
                               size_t                  high,
                               size_t                  max_depth,
                               crappysort_Comparator_f comparator)
{
    if((high - low) <= 1)
    {
        return;
    }
    else if(max_depth == 0)
    {
        crappysort_InsertionSort(data + low, high - low + 1, comparator);
    }
    else
    {
        size_t pivot;
        pivot = crappysort_Partition(data, low, high, comparator);

        crappysort_IntroSortRec__(data, low, pivot, max_depth - 1, comparator);
        crappysort_IntroSortRec__(data, pivot + 1, high, max_depth - 1, comparator);
    }
}

void crappysort_IntroSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator)
{
    if(data == NULL || comparator == NULL)
    {
        return;
    }

    /*
    procedure sort(A : array):
        let maxdepth = ⌊log(length(A))⌋ × 2  // gnu uses 2×log2 n
        introsort(A, maxdepth)

    procedure introsort(A, maxdepth):
        n ← length(A)
        if n ≤ 1:
            return  // base case
        else if maxdepth = 0:
            InsertionSor(A)
        else:
            p ← partition(A)  // assume this function does pivot selection, p is the final position of the pivot
            introsort(A[0:p-1], maxdepth - 1)
            introsort(A[p+1:n], maxdepth - 1)
    */

    if(crappysort_IsSorted(data, len, comparator) == 1)
    {
        return;
    }

    size_t max_depth = ((size_t)log(len)) * 2; // ⌊log2(length(A))⌋ × 2;  // gnu uses 2×log2 n
    crappysort_IntroSortRec__(data, 0, len - 1, max_depth, comparator);
}

/* End intro sort */

/* Insertion sort */

void crappysort_InsertionSort(uint64_t* data, size_t len, crappysort_Comparator_f comparator)
{
    if(data == NULL || comparator == NULL)
    {
        return;
    }

    /*
    i ← 1
    while i < length(A)
        x ← A[i]
        j ← i - 1
        while j >= 0 and A[j] > x
            A[j+1] ← A[j]
            j ← j - 1
        end while
        A[j+1] ← x[3]
        i ← i + 1
    end while
    */

    size_t i = 1;
    while(i < len)
    {
        uint64_t x = data[i];
        ssize_t  j = i - 1; /* sale, todo refaire avec des unsigned*/

        while(j >= 0 && comparator(data[j], x) > 0)
        {
            data[j + 1] = data[j];
            --j;
        }

        data[j + 1] = x;
        ++i;
    }
}

/* End insertion sort */

/* STD qsort */

/*
void crappysort_STDQsort(uint64_t* data,
                         size_t len,
                         crappysort_Comparator_f comparator)
{

}
*/

/* end STD Qsort */
